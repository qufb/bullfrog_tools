#!/usr/bin/env python3

import hashlib
import re
import struct
import sys

# TODO: Detect submenu entries for corresponding menu entries
# FIXME: Needle misses first menus from some builds
DEBUG_MENU_NEEDLE_PAD_LEN = 0x80
DEBUG_MENU_NEEDLE = (
    (b"\x00" * DEBUG_MENU_NEEDLE_PAD_LEN)
    + b"([\x00|\x01])\x00([\x01-\x05])\x00\x00\x00\x00\x00\x00\x00(([\x00|\x01][\x01-\xff])|(\x01[\x00|\x01]))"
)
MENU_LEN = 0x1A  # FIXME: Only known at runtime, but we can use the debug menu value, which is the largest one
PATTERNS = {
    "886f53454f4ea2df0e9f85e4ab01fdecf4c81b27": (
        "DOS Demo, Beta v2.0 (LE Unstubbed)",
        0xC,
        0x12,
        0x2,
    ),
    "d37aaea89e22be35467b3050280394c74a5e0b07": (
        "DOS Demo, Beta v2.0",
        0xC,
        0x12,
        0x2,
    ),
    "2da7f5e9265e30d025de2ee40d1f6ae95d3c01b8": (
        "Windows Demo, Beta v2.0",
        0xC,
        0x12,
        0x2,
    ),
    "103567d488cff36cb6feaa38138563cd190c411a": ("DOS Retail, Beta 4", 0xC, 0x13, 0x3),
    "217a03f6c28a78e5d23685c8ec4c412560f73718": (
        "Windows Retail, Beta 4",
        0xC,
        0x13,
        0x3,
    ),
    "d108d34d4e435ce4ad1e9c9b76ab3ad6f000d6ae": ("DOS Retail, Beta 5", 0xD, 0x14, 0x3),
    "83e0e9d9b8f08d361fdfe290796340a727a26b72": (
        "Windows Retail, Beta 5",
        0xD,
        0x14,
        0x3,
    ),
}

file_name = sys.argv[1]
with open(file_name, "rb") as f:
    input_bytes = bytearray(f.read())

hash_object = hashlib.sha1(input_bytes)
file_hash = hash_object.hexdigest()
if file_hash not in PATTERNS:
    print(f"Could not find file in database, sha1 was {file_hash}.")
    exit(1)
build_patterns = PATTERNS[file_hash]
build_name = build_patterns[0]
next_fun_offset = build_patterns[1]
next_entry_offset = build_patterns[2]
extra = build_patterns[3]
print(f"Matched build: {build_name}")

menu_re = re.compile(DEBUG_MENU_NEEDLE, re.MULTILINE)
for menu_match in menu_re.finditer(input_bytes):
    menu_offset = menu_match.start() + DEBUG_MENU_NEEDLE_PAD_LEN
    if menu_offset > 0xF000:
        print(f"Found menu_offset: {hex(menu_offset)}")
    else:
        print(f"Skipping bad offset: {hex(menu_offset)}...")
        continue

    seen_mem_addrs = set()
    seen_unk_addrs = set()
    menu_entry_offset = menu_offset + 0xA
    menu_entry_is_active = input_bytes[menu_entry_offset : menu_entry_offset + 1]
    menu_entry_offset += extra
    for i in range(MENU_LEN):
        menu_entry_mem_addr = struct.unpack(
            "<I", input_bytes[menu_entry_offset : menu_entry_offset + 4]
        )[0]
        seen_mem_addrs.add(menu_entry_mem_addr)

        menu_entry_unk_addr = struct.unpack(
            "<I", input_bytes[menu_entry_offset + 4 : menu_entry_offset + 4 + 4]
        )[0]
        seen_unk_addrs.add(menu_entry_unk_addr)

        has_submenu = input_bytes[menu_entry_offset + 8 : menu_entry_offset + 9]

        menu_entry_fun_offset = menu_entry_offset + next_fun_offset
        menu_entry_fun_addr = struct.unpack(
            "<I", input_bytes[menu_entry_fun_offset : menu_entry_fun_offset + 4]
        )[0]

        print(
            f"???: {hex(menu_entry_unk_addr).ljust(8)} | mem: {hex(menu_entry_mem_addr).ljust(8)} | fun: {hex(menu_entry_fun_addr).ljust(8)} | has_sub:{int.from_bytes(has_submenu, 'little')} | enabled:{int.from_bytes(menu_entry_is_active, 'little')}"
        )

        menu_entry_offset += next_entry_offset - extra
        menu_entry_is_active = input_bytes[menu_entry_offset : menu_entry_offset + 1]
        menu_entry_offset += extra

    for addr in seen_mem_addrs:
        if addr == 0:
            continue

        addr_re = re.compile(re.escape(struct.pack("<I", addr)), re.MULTILINE)
        matches = list(addr_re.finditer(input_bytes))
        if len(matches) > 1:
            print(
                f"Found {len(matches)} matches for mem addr {hex(addr)}: {','.join(map(lambda x: hex(x.start()), matches))}"
            )
