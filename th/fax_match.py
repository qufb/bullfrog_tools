#!/usr/bin/env python3

import sys

with open(sys.argv[1], "rb") as f:
    data = f.read()

needle = [2, 4, 3, 2, 8]
match_i = 1
for i in range(len(data) - len(needle)):
    le_bytes = bytearray(data[i : i + len(needle)])
    be_bytes = bytearray(data[i : i + len(needle)])[::-1]
    for candidate in (le_bytes, be_bytes):
        for j in range(len(needle)):
            if abs(candidate[j] - candidate[j + 1]) == abs(needle[j] - needle[j + 1]):
                match_i += 1
                if match_i == len(needle):
                    print(hex(i))
                    match_i = 1
                    break
            else:
                match_i = 1
                break

# Beta 4: 0xc1358
# Beta 5: 0xc2d60
