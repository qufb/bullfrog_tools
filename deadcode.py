#!/usr/bin/env python3

# Usage:
# ./deadcode.py foo.wdump.txt foo.le.exe

from wdump_to_r2 import dump
import r2pipe
import re
import sys


def parse_functions(r2p, filename, needle=None):
    functions = r2p.cmdj("aflj")
    parsed_functions = []
    for f in functions:
        if f["name"].startswith("sym.imp."):
            # Skip imports
            continue

        if needle and f["offset"] != needle:
            # Skip unmatched offsets
            continue

        xrefs = []
        for xref in r2p.cmdj(f"axtj @{f['offset']}"):
            xrefs.append(xref)
        parsed_functions.append(
            {
                "name": f["name"],
                "size": f["size"],
                "offset": f["offset"],
                "xrefs": xrefs,
            }
        )

    return parsed_functions


def find_no_direct_calls(functions):
    for func in functions:
        if len(func["xrefs"]) < 1:
            print(
                "No direct calls to function: {} @ {}".format(
                    func["name"], hex(func["offset"])
                )
            )


def find_undefined_blocks(r2p, functions):
    # TODO: Parse r-x .text section (it is split into pages)
    block_start = min(functions, key=lambda x: int(x["offset"]))
    block_end = max(functions, key=lambda x: int(x["offset"]))
    text_start = block_start["offset"]
    text_end = block_end["offset"] + block_end["size"]

    bodies = []
    for func in functions:
        bodies.append([int(func["offset"]), int(func["offset"]) + int(func["size"])])
    bodies.sort(key=lambda x: x[0])

    extra_re = re.compile(b"^((\x00*)|(\xcc*))$", re.MULTILINE)
    target_block = text_start
    for body in bodies:
        if target_block > text_end:
            break

        # Skip over small blocks
        offset_diff = abs(target_block - body[0])
        if offset_diff > 6:
            # Skip over junk blocks
            target_bytes = bytearray(
                r2p.cmdj(f"pxj {offset_diff} @ {hex(target_block)}")
            )
            if len(extra_re.findall(target_bytes)) == 0:
                # Identify functions by matched first instruction with mnemonic `push`
                dis_block = r2p.cmdj(f"pDj {offset_diff} @ {hex(target_block)}")
                opcodes = list(
                    filter(
                        lambda x: not (
                            x == "" or x.startswith("align") or x.startswith("int")
                        ),
                        map(lambda x: x["opcode"] if "opcode" in x else "", dis_block),
                    )
                )
                push_opcodes = [x.startswith("push") for x in opcodes]
                if len(push_opcodes) > 0 and push_opcodes[0]:
                    push_str = "(Starts with `push`)"
                else:
                    push_str = ""

                print(
                    "Undefined block at: {}, size: {} {}".format(
                        hex(target_block), offset_diff, push_str
                    )
                )

        target_block = body[1]


if __name__ == "__main__":
    filename_dump = sys.argv[1]
    cmds = dump(open(filename_dump, "rb").readlines())

    filename_bin = sys.argv[2]
    r2p = r2pipe.open(filename_bin)
    for cmd in cmds:
        r2p.cmd(cmd)
    r2p.cmd("aaa")

    functions = parse_functions(r2p, filename_bin)
    find_no_direct_calls(functions)
    find_undefined_blocks(r2p, functions)
