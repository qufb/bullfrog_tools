#!/usr/bin/env python3

import re
import sys

segment_offsets = []
objects = []
name = None
is_object = False
is_next = False
for line in sys.stdin.buffer.readlines():
    line = line.decode("latin-1").strip()

    for match in re.finditer(
        r"^[ \t]*relocation base address[ \t]*=[ \t]*([0-9A-F]+)H", line
    ):
        segment_offsets.append(int(match.groups()[0], 16))

    for match in re.finditer(r"^[ \t]*Name:[ \t]*([^ \t]+)", line):
        name = match.groups()[0]
        is_object = True
        is_next = True

    if is_next:
        is_next = False
        continue

    try:
        if is_object:
            for match in re.finditer(
                r"^[ \t]*address[ \t]*=[ \t]*([^ \t:]+):([^ \t]+)", line
            ):
                segment_index = int(match.groups()[0].lstrip("0")) - 1
                segment_offset = segment_offsets[segment_index]
                object_offset = int(match.groups()[1].lstrip("0"), 16)
                objects.append([name, hex(segment_offset + object_offset)])
    except:
        pass

    is_object = False

for o in objects:
    print(f'MakeName({o[1]}, "{o[0]}");')
