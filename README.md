# bullfrog_tools

Tools for interoperability with file formats used in the game "Theme Hospital", but can be adapted for others games by Bullfrog Productions with Watcom compiled executables.

For more context, read the [accompanying writeup](https://qufb.gitlab.io/writeups/2021/06/02/A-byte-too-far).

Disclaimer: This project is not associated with Bullfrog Productions or its current copyright owners.
