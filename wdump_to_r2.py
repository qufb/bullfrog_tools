#!/usr/bin/env python3

# Usage:
# . ~/.local/bin/openwatcom
# wdump -Dx -a HOSPITAL.EXE | ./wdump_to_r2.py

import re
import sys


def dump(lines):
    segment_offsets = []
    objects = []
    name = None
    is_object = False
    is_next = False
    for line in lines:
        line = line.decode("latin-1").strip()

        for match in re.finditer(
            r"^[ \t]*relocation base address[ \t]*=[ \t]*([0-9A-F]+)H", line
        ):
            segment_offsets.append(int(match.groups()[0], 16))

        for match in re.finditer(r"^[ \t]*Name:[ \t]*([^ \t]+)", line):
            name = match.groups()[0]
            is_object = True
            is_next = True

        if is_next:
            is_next = False
            continue

        try:
            if is_object:
                for match in re.finditer(
                    r"^[ \t]*address[ \t]*=[ \t]*([^ \t:]+):([^ \t]+)", line
                ):
                    segment_index = int(match.groups()[0].lstrip("0")) - 1
                    segment_offset = segment_offsets[segment_index]
                    object_offset = int(match.groups()[1].lstrip("0"), 16)
                for match in re.finditer(r"^[ \t]*kind:[ \t]*\(.*", line):
                    objects.append(
                        [name, hex(segment_offset + object_offset), "code" in line]
                    )
                    is_object = False
        except BaseException:
            pass

    cmds = []
    for o in objects:
        name, offset, is_code = o
        name = re.sub(r"W?([^?]\+?)\?([^$]*)[^ ]*", r"\2", name)
        name = re.sub(r"[^a-zA-Z0-9_ ]+", r"_", name)
        if is_code:
            cmds.append(f"af sym.{name} @ {offset}")
        else:
            cmds.append(f"f sym.{name} @ {offset}")

    return cmds


if __name__ == "__main__":
    cmds = dump(sys.stdin.buffer.readlines())
    for cmd in cmds:
        print(cmd)
